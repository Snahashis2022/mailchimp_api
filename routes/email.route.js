const express = require('express');
const router = express.Router();
const EmailService = require('./../services/mailchimp.service')
const TemplateSevice = require('./../services/email-template.service');

router.get('/', async ({body}, res) => {
  const {
    email,
    firstName,
    lastName,
    message
  } = body;
  const notValidBody = !email||!firstName||!lastName||!message;
  if(notValidBody){
    console.error("Bad Request Exception");
    return res.status(400).json({
      error:"Bad Request Exception", 
      message: `email, firstName, lastName, message are required`});
  } 
  const emailService = new EmailService(process.env.MANDRILLA_API_KEY);
  const template = new TemplateSevice();
  const content = template.getTestTemplate({
    firstName,
    lastName,
    message,
  });
  console.log(content);
  const resp = await emailService.sendMail(email, "Test Subject", content);
  return res.status(200).json(resp);
});

module.exports = router;