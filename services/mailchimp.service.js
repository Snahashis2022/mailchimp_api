const chimp = require("@mailchimp/mailchimp_transactional");
class MailchimpService {
  mailchimp;

  constructor(apiKey) {
    this.mailchimp = chimp(apiKey);
  }

  async sendMail(email, subject, body) {
    try {
      const message = {
        subject: subject,
        from_email: "mcspraint954@gmail.com",
        html: body,
        to: [
          {
            email: email,
            
          },
        ],
      };
      const response = await this.mailchimp.messages.send({
        message: message,
      });
      return response;
    } catch (error) {
      console.error(error);
      throw new Error("Failed to send email");
    }
  }
}

module.exports = MailchimpService;
