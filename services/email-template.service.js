
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");

class EmailTemplateService {
  compileFile(filePath, variables) {
    const expectedFileType = [".hbs"];
    if (!expectedFileType.includes(path.extname(filePath))) {
      throw console.error(`File type shoud be hbs`);
    }
    const content = fs.readFileSync(filePath, "utf8");
    const template = handlebars.compile(content);
    return template(variables);
  }

  getRootPathOfTemplate(filename) {
    const pathToTemplate = path.join(
      __dirname,
      "./../",
      `public/templates/${filename}.hbs`
    );
    return pathToTemplate;
  }

  getTemplate(templateName, variables) {
    const pathToTestMailTemplate = this.getRootPathOfTemplate(templateName);
    const template = this.compileFile(pathToTestMailTemplate, variables);
    return template;
  }

  getTestTemplate(variables) {
    const template = this.getTemplate(
      "test-email.template",
      variables
    );
    return template;
  }

}

module.exports = EmailTemplateService;
